package com.vti.finalexamservice.controller;

import com.vti.finalexamservice.service.IAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/auth")
public class AuthController {
    @Autowired
    private IAuthService authService;

    @PostMapping("/login")
    ResponseEntity search(@RequestParam String username, @RequestParam String password) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(authService.login(username, password));
    }

}
