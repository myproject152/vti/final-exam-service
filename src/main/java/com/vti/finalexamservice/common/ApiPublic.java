package com.vti.finalexamservice.common;

import java.util.ArrayList;
import java.util.List;

public class ApiPublic {
    public static List<String> get() {
        List<String> rs = new ArrayList<>();
        rs.add("/v3/api-docs");
        rs.add("your-app-root/api/v2/api-docs");
        rs.add("/api/v1/auth/login");
        rs.add("/swagger-ui.html");
        rs.add("/swagger-ui-custom.html");
        rs.add("swagger-ui/index.html");
        rs.add("/configuration/security");
        rs.add("/swagger-ui.html");
        rs.add("/swagger-ui");
//        rs.add("");
//        rs.add("");
//        rs.add("");

        return rs;
    }
}
