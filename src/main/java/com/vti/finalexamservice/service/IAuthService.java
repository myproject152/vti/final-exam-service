package com.vti.finalexamservice.service;

import com.vti.finalexamservice.model.dto.LoginResponse;

public interface IAuthService {
    LoginResponse login(String username, String password);
}
