package com.vti.finalexamservice.service.impl;

import com.vti.finalexamservice.config.exceptions.AppException;
import com.vti.finalexamservice.config.exceptions.ErrorResponseBase;
import com.vti.finalexamservice.config.sercurity.model.SysUserDetails;
import com.vti.finalexamservice.config.token.JWTTokenUtils;
import com.vti.finalexamservice.model.dto.LoginResponse;
import com.vti.finalexamservice.model.entity.Account;
import com.vti.finalexamservice.repository.AccountRepository;
import com.vti.finalexamservice.service.IAuthService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AuthService implements IAuthService {
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    HttpServletRequest request;
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public LoginResponse login(String username, String password) {
        Optional<Account> user = accountRepository.findByUsername(username);
        SysUserDetails sysUserDetails = new SysUserDetails();
        LoginResponse response = new LoginResponse();
        if (user.isEmpty()) {
            throw new AppException(ErrorResponseBase.NOT_EXISTED_ACCOUNT);
        }
        if (!encoder.matches(password, user.get().getPassword())) {
            throw new AppException(ErrorResponseBase.LOGIN_FALSE);
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        BeanUtils.copyProperties(user.get(), sysUserDetails);
        authorities.add(new SimpleGrantedAuthority(user.get().getRole().name()));
        sysUserDetails.setAuthorities(authorities);

        String token = JWTTokenUtils.createAccessToken(sysUserDetails);

        // Cơ chế lưu token
        JWTTokenUtils.setTokenInfo(token, sysUserDetails.getUsername(), request.getHeader("User-Agent"));

        response.setToken(token);
        response.setRole(user.get().getRole().name());
        return response;
    }
}
