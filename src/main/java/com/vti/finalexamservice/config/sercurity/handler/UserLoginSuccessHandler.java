package com.vti.finalexamservice.config.sercurity.handler;

import com.vti.finalexamservice.config.exceptions.ResponseAuthentication;
import com.vti.finalexamservice.config.sercurity.model.SysUserDetails;
import com.vti.finalexamservice.config.token.JWTTokenUtils;
import com.vti.finalexamservice.model.dto.LoginResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class UserLoginSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        // Lây đối tượng user đã đăng nhập
        SysUserDetails sysUserDetails = (SysUserDetails) authentication.getPrincipal();
        sysUserDetails.setIp(request.getHeader("User-Agent"));

        // Tạo ra token
        String token = JWTTokenUtils.createAccessToken(sysUserDetails);

        // Cơ chế lưu token
        JWTTokenUtils.setTokenInfo(token, sysUserDetails.getUsername(), request.getHeader("User-Agent"));

        LoginResponse loginResponse = new LoginResponse(sysUserDetails.getRole().name(), token);
        ResponseAuthentication.responseSuccess(response, loginResponse);
    }
}
