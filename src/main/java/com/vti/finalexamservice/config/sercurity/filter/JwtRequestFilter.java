package com.vti.finalexamservice.config.sercurity.filter;

import com.vti.finalexamservice.config.sercurity.model.SysUserDetails;
import com.vti.finalexamservice.config.token.JWTConfig;
import com.vti.finalexamservice.config.token.JWTTokenUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {
//    @Autowired
//    private StringRedisTemplate redisTemplate;

    //    public JwtRequestFilter(AuthenticationManager authenticationManager) {
//        super(authenticationManager);
//    }
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        // Lấy token từ api
        String header = httpServletRequest.getHeader(JWTConfig.tokenHeader);
        String request = httpServletRequest.getRequestURI();
        if (StringUtils.containsAnyIgnoreCase(request, "/login")
                || StringUtils.containsAnyIgnoreCase(request, "/swagger-ui")
                || StringUtils.containsAnyIgnoreCase(request, "/swagger-resources")
                || StringUtils.containsAnyIgnoreCase(request, "/v3/api-docs")) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } else {
            if (JWTTokenUtils.checkToken(header, httpServletResponse, httpServletRequest)) {
                // Giải mã token -> lấy thông tin user -> authen
                SysUserDetails sysUserDetails = JWTTokenUtils.parseAccessToken(header);

                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        sysUserDetails, sysUserDetails.getId(), sysUserDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);

                JWTTokenUtils.updateExpiration(header);
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }
        }
    }
}
